
var k = 8; //可以猜测的次数
var correct = false; //判断正确
var base = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]; //随机的数组
var start = parseInt(Math.random() * 6,10); //取数组的开始位置
var num = 0;//已猜测的数
window.setArray=[];
//打乱数组
base.sort(function () {
    return Math.random() > .5 ? -1 : 1;
});
//取4位随机字符串生成答案的数组
setArray = base.slice(start, start + 4);


function g(id) {
    return document.getElementById(id);
}
window.printText = function (text) {
    g("result").innerHTML += "<p>"+ text +"</p>";
}
window.surrender = function () {
    printText("答案是：" + setArray.join(""));
}
function filterStr(str) {
    var ar2 = str.split("");
    var array = new Array();
    var j = 0
    for (var i = 0; i < ar2.length; i++) {
        if ((array == "" || array.toString().match(new RegExp(ar2[i], "g")) == null) && ar2[i] != "") {
            array[j] = ar2[i];
            array.sort();
            j++;
        }
    }
    return array;
}

window.guess = function () {
    if (num < k) {
        var inputValue = g("number").value.replace(/\s/g, '').substring(0, 4);//去除所有空格并截取4位
        var inputArray = inputValue.split("");
        if (inputArray.length == 0) {
            printText("请输入4位不重复的数字");
            return;
        }
        if (filterStr(inputValue).length == 4) {
            var result = check(inputArray, setArray);
            if (result == '4A0B')
                printText("恭喜答对了！");
            var para = document.createElement("li");
            var node = document.createTextNode(inputArray.join("") + "\t" + result);
            para.appendChild(node);
            var element = document.getElementById("matchlist");
            element.appendChild(para);
        }
        else {
            printText("输入有误，不能有重复的数字");
        }
    } else {
        printText("8次还没有猜出来么？答案是：" + setArray.join(""));
    }
}
